<?php

require "../../carpeta/index.php";
// $factorialDe = -1;

function factorial($numero){
  if($numero < 0){
    return "El número debe ser mayor igual que 0";
  }
  if($numero === 0){
    return 1;
  }
  return $numero * factorial($numero - 1);
}

// echo "El factorial de $factorialDe = ".factorial($factorialDe);

// número variable de argumentos
function sumar($nombre, ...$numeros){
  $suma = 0;
  for($i = 0; $i < count($numeros); $i++){
    $suma = $suma + $numeros[$i];
  }
  $numerosDeArgumentos = func_num_args();
  echo "Número de argumentos de la función: ".$numerosDeArgumentos;
  return "$nombre has sumado= ". $suma;
}
// echo sumar("Ruben",4,6,8,9,1212,10);
