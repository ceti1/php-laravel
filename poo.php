<?php

$nombre = "Juan José";
$apellidoPaterno = "Perez";
$apellidoMaterno = "Ruíz";
$fechaNacimiento = "1991-06-30";

function nombreCompleto($nombre, $apellidoPaterno, $apellidoMaterno)
{
  return "$apellidoPaterno $apellidoMaterno $nombre";
}


function calcularEdad($fechaNacimiento)
{
  // calcular la fecha nacimiento
  return "20";
}

class Persona
{
  public $nombre;
  public $apellidoPaterno;
  public $apellidoMaterno;
  public $fechaNacimiento;
  public $sexo;

  public function __construct($nom, $apePat, $apeMat, $fechNac)
  {
    $this->nombre = $nom;
    $this->apellidoPaterno = $apePat;
    $this->apellidoMaterno = $apeMat;
    $this->fechaNacimiento = $fechNac;
  }

  public function nombreCompleto()
  {
    $nombrecompleto = $this->apellidoPaterno . " " . $this->apellidoMaterno . ", " . $this->nombre;
    return $nombrecompleto;
  }
}


$juan = new Persona("Juan", "Perez", "Solis", "2020-01-01");
$pedro = new Persona("Pedro", "Saenz", "Sanchez", "2019-02-28");

echo "<pre>";
// como llamar a un atributo
var_dump($juan->nombre);

// como llamar a un metodo
var_dump($juan->nombreCompleto());
echo "</pre>";

// var_dump($juan);
