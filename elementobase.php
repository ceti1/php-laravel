<?php

class ElementoBase
{
  public $titulo;
  public $descripcion;
  public $duracion;
  public $visible;
  public $logros;

  public function __construct($titulo, $descripcion, $duracion, $visible)
  {
    $this->titulo = $titulo;
    $this->descripcion = $descripcion;
    $this->duracion = $duracion;
    $this->visible = $visible;
  }

  public function nombre()
  {
    return "$this->titulo: " . substr($this->descripcion, 0, 20);
  }
}
