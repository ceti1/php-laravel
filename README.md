## Curso de PHP y Laravel - CETI

Plantilla de proyecto

Diapositivas de las clases

- Clase 00: [Introducción](https://docs.google.com/presentation/d/1TgV6BIN9xNBPpC_wHRBtO5pQmqE5TTL0ZvOqH8QTxDU/edit?usp=sharing).
- Clase 01: [Estructuras de control](https://docs.google.com/presentation/d/1HsO5YBPeEJqXCT8C7uftwuBeJ97BnzNLTb5xmVexEyI/edit?usp=sharing).
- Clase 02.1: [Funciones y Agregando archivos externos](https://docs.google.com/presentation/d/1l6zGXiE8HHmkGGxUOTs7pYsAbTiFON3noKUx9HthNkI/edit?usp=sharing).
- Clase 02.2: [Programación orientada a objetos](https://docs.google.com/presentation/d/1weBH2MatYtSAiSFXtl66BQPS5n0_eUZrWmlUiqMX7zw/edit?usp=sharing).
- Clase 03.1: [Herencia, Polimorfismo, Interfaces](https://docs.google.com/presentation/d/1Uryrx3OtqagsBkYGEvVh3Psn4H9SvbPClF3LXEhAbao/edit?usp=sharing).
- Clase 03.2: [Herramientas para proyectos PHP](https://docs.google.com/presentation/d/1G0q9BxtA6MlFKxZnlHeZXqSDnuam7Mh6qGQQMUAAMXk/edit?usp=sharing).
