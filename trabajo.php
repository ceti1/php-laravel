<?php

require_once "elementobase.php";

class Trabajo extends ElementoBase
{
  public static $sueldo = "1800";

  public function nombre()
  {
    return $this->titulo . " trabajé" . $this->duracion . " meses";
  }

  public static function salarioAnual()
  {
    return self::$sueldo * 12;
  }
}
