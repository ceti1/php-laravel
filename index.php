<?php
require_once "./trabajo.php";

$descripcion = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sapiente sed pariatur sint exercitationem eos expedita eveniet veniam ullam, quia neque facilis dicta voluptatibus. Eveniet doloremque ipsum itaque obcaecati nihil.";
$php = new Trabajo("Desarrollador PHP", $descripcion, 14, true);
$java = new Trabajo("Desarrollador Java", $descripcion, 7, false);
$python = new Trabajo("Desarrollador Python", $descripcion, 18, true);
$javascript = new Trabajo("Dev javascript", $descripcion, 4, true);

$listaTrabajos = [
    $php,
    $java,
    $python,
    $javascript
];

$sueldophp = $php::$sueldo;
$sueldojava = $java::$sueldo;
$sueldopython = $python::$sueldo;
var_dump($sueldophp);
var_dump($sueldojava);
var_dump($sueldopython);

var_dump(Trabajo::salarioAnual());
var_dump(Trabajo::$sueldo);

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Currículum</title>
</head>

<body>
    <div class="container">
        <div id="resume-header" class="row">
            <div class="col-3">
                <img class="profile-picture" src="https://ui-avatars.com/api/?name=Juan+Perez&size=255" alt="">
            </div>
            <div class="col">
                <h1><?php echo "Juan Perez"; ?></h1>
                <h2><?php echo "Desarrollador" ?></h2>
                <ul>
                    <li>Email: jperez@mail.com</li>
                    <li>Teléfono: </li>
                    <li>LinkedIn: https://linkedin.com</li>

                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h2 class="border-bottom-gray">Resumen de la carrera</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div>
                    <h3 class="border-bottom-gray">Experiencia laboral</h3>
                    <ul>
                        <?php
                        foreach ($listaTrabajos as $trabajo) {
                        ?>
                            <?php
                            if ($trabajo->visible === true) {
                            ?>
                                <li class="work-position">
                                    <h5><?php echo $trabajo->nombre(); ?></h5>
                                    <p><?php echo $trabajo->descripcion; ?></p>
                                    <p>Duración: <?php echo $trabajo->duracion; ?></p>
                                    <strong>Logros:</strong>
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>
                                        <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>
                                        <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>
                                    </ul>
                                </li>
                            <?php
                            }
                            ?>
                        <?php
                        }
                        ?>

                    </ul>
                </div>
                <div>
                    <h3 class="border-bottom-gray">Proyectos</h3>
                    <div class="project">
                        <h5>Project X</h5>
                        <div class="row">
                            <div class="col-3">
                                <img class="profile-picture" src="https://ui-avatars.com/api/?name=John+Doe&size=255" alt="">
                            </div>
                            <div class="col">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius earum corporis at
                                    accusamus
                                    quisquam hic quos vel? Tenetur, ullam veniam consequatur esse quod cum, quam
                                    cupiditate
                                    assumenda natus maiores aperiam.</p>
                                <strong>Tecnologías utilizadas:</strong>
                                <span class="badge badge-secondary">PHP</span>
                                <span class="badge badge-secondary">HTML</span>
                                <span class="badge badge-secondary">CSS</span>
                            </div>
                        </div>
                    </div>
                    <div class="project">
                        <h5>Project X</h5>
                        <div class="row">
                            <div class="col-3">
                                <img class="profile-picture" src="https://ui-avatars.com/api/?name=John+Doe&size=255" alt="">
                            </div>
                            <div class="col">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius earum corporis at
                                    accusamus
                                    quisquam hic quos vel? Tenetur, ullam veniam consequatur esse quod cum, quam
                                    cupiditate
                                    assumenda natus maiores aperiam.</p>
                                <strong>Tecnologías utilizadas:</strong>
                                <span class="badge badge-secondary">PHP</span>
                                <span class="badge badge-secondary">HTML</span>
                                <span class="badge badge-secondary">CSS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <h3 class="border-bottom-gray"> Habilidades y Herramientas</h3>
                <h4>Backend</h4>
                <ul>
                    <?php
                    foreach ($backend as $indice => $fila) {
                        if ($indice === "lenguajePython") {
                            continue;
                        }

                        if ($fila === 'Java') {
                            break;
                        }
                    ?>
                        <li><?php echo $fila; ?></li>
                    <?php
                    }
                    ?>
                </ul>
                <h4>Frontend</h4>
                <ul>
                    <?php
                    for ($contadorFor = 0; $contadorFor < count($frontend); $contadorFor++) {
                    ?>
                        <li><?php echo $frontend[$contadorFor]; ?></li>
                    <?php
                    }
                    ?>
                </ul>
                <h4>Frameworks</h4>
                <ul>
                    <?php
                    $contadorDoWhile = 0;
                    do {
                    ?>
                        <li><?php echo $frameworks[$contadorDoWhile] ?></li>
                    <?php
                        $contadorDoWhile = $contadorDoWhile + 1;
                    } while ($contadorDoWhile < count($frameworks));
                    ?>
                </ul>
                <h3 class="border-bottom-gray">Idiomas</h3>
                <ul>
                    <?php
                    $contadorWhile = count($idiomas) - 1;
                    while ($contadorWhile >= 0) {
                    ?>
                        <li><?php echo $idiomas[$contadorWhile]; ?> </li>
                    <?php
                        $contadorWhile = $contadorWhile - 1;
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div id="resume-footer" class="row">
            <div class="col">
                Designed by @juanperez
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>

</html>